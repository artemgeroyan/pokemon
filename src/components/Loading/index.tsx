import React from 'react';
import { Spinner, LoadBlock } from './Loading.style';

const Loading = () => (
  <LoadBlock>
    <Spinner />
  </LoadBlock>
);

export default Loading;
