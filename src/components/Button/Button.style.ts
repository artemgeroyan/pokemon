import styled from 'styled-components';

interface Props {
  isGreen: boolean;
  isLarge: boolean;
}

export const ButtonStyle = styled.button`
  border: none;
  background: ${(props: Props) => (props.isGreen ? '#73d677' : '#f2cb07')};
  box-shadow: inset 0 -9px 0 rgba(0, 0, 0, 0.18);
  border-radius: 14px;
  height: 66px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20px 9px;
  cursor: pointer;
  font-weight: bold;
  font-size: 23px;
  color: #212121;
  outline: none;
  width: ${(props: Props) => props.isLarge && '100%'};
  &:active {
    box-shadow: inset 0 -6px 0 rgba(0, 0, 0, 0.18);
    padding-bottom: 6px;
  }
`;
