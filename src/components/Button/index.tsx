import React from 'react';
import { ButtonStyle } from './Button.style';

interface ButtonProps {
  // eslint-disable-next-line no-unused-vars
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  isLarge?: boolean;
  isGreen?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  children,
  onClick,
  isLarge = false,
  isGreen = false,
}) => (
  <ButtonStyle isGreen={isGreen} isLarge={isLarge} onClick={onClick}>
    {children}
  </ButtonStyle>
);

export default Button;
