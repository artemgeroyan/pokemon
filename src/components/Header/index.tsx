import React from 'react';
import { useLocation } from 'react-router-dom';

import {
  Root, Wrap, PokemonLogo, MenuWrap, MenuLink,
} from './Header.style';
import { GENERAL_MENU } from '../../typing';
import { ReactComponent as PokemonLogotype } from './assets/logo.svg';

const Header: React.FC = () => {
  const location = useLocation();
  return (
    <Root>
      <Wrap>
        <PokemonLogo>
          <PokemonLogotype />
        </PokemonLogo>
        <MenuWrap>
          {GENERAL_MENU.map(({ title, link }) => (
            <MenuLink key={title} to={link} active={location.pathname === link}>
              {title}
            </MenuLink>
          ))}
        </MenuWrap>
      </Wrap>
    </Root>
  );
};

export default React.memo(Header);
