import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const Root = styled.div`
  display: flex;
  justify-content: center;
  height: 93px;
  align-items: center;
  background: #f5db13;
  box-shadow: 0px 4px 16px rgba(1, 28, 64, 0.2);
`;

export const Wrap = styled.div`
  width: 100%;
  max-width: 1125px;
  height: 63px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const PokemonLogo = styled.div`
  width: 158px;
  height: 63px;
`;

export const MenuWrap = styled.div`
  display: flex;
  align-items: center;
  max-width: 658px;
  width: 100%;
  justify-content: space-between;
`;

interface MenuLLinkProps {
  active: boolean;
}

export const MenuLink = styled(Link)<MenuLLinkProps>`
  font-size: 25px;
  color: #212121;
  text-decoration: none;
  position: relative;

  &:after {
    content: '';
    display: block;
    position: absolute;
    bottom: -8px;
    height: 1px;
    width: 100%;
    background: #212121;
    border: 1px solid #212121;
    border-radius: 6px;
    opacity: 0;
    left: 50%;
    transform: translate(-50%, 0);
    transition: opacity, width ease-in-out 0.25s;
  }

  &:hover {
    &:after {
      width: 100%;
      opacity: 1;
    }
  }

  ${props => props.active
    && css`
      &:after {
        content: '';
        display: block;
        position: absolute;
        bottom: -8px;
        height: 1px;
        width: 100%;
        background: #212121;
        border: 1px solid #212121;
        border-radius: 6px;
        opacity: 1;
        left: 50%;
        transform: translate(-50%, 0);
        transition: opacity, width ease-in-out 0.25s;
      }
    `}
`;
