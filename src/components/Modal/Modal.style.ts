import styled from 'styled-components';

const BACKGROUND_COLOR_IMG: any = {
  water: 'linear-gradient(270deg, #5BC7FA 0.15%, #35BAFF 100%)',
  fire: 'linear-gradient(270deg, #b33327 0.15%, #d93e30 100%)',
  grass: 'linear-gradient(270deg, #64D368 0.15%, #64D368 70.88%)',
};

const BACKGROUND_COLOR: any = {
  water: 'linear-gradient(180deg, #35BAFF 42.19%, #A2CFF0 100%)',
  fire: 'linear-gradient(180deg, #732119 42.19%, #D93E30 100%)',
  grass: 'linear-gradient(180deg, #70A83B 42.19%, #64D368 100%)',
};

export const ModalBox: any = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
  transform: ${(props: any) => (props.active ? 'scale(1)' : 'scale(0)')};
`;

export const ModalContent = styled.div`
  border-radius: 12px;
  background-color: white;
  width: 797px;
  height: 372px;
  display: flex;
`;

export const ModalInfo: any = styled.div`
  background: ${(props: any) => BACKGROUND_COLOR[props.type]};
  z-index: 10;
  width: 454px;
  border-radius: 0 12px 12px 0;
`;

export const PictureWrap: any = styled.div`
  width: 343px;
  height: 100%;
  background: ${(props: any) => BACKGROUND_COLOR_IMG[props.type]};
  box-shadow: 4px 4px 8px rgba(1, 28, 64, 0.2);
  /* position: absolute;
  right: 0; */
  display: flex;
  border-radius: 12px 0 0 12px;
  z-index: 11;
  /* justify-content: center; */
  /* align-items: center; */

  img {
    width: 100%;
  }
`;
