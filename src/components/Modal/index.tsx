import React from 'react';
import {
  ModalBox, ModalContent, PictureWrap, ModalInfo,
} from './Modal.style';

const Modal = ({ active, setModalActive, detailPokemon }) => (
  <ModalBox onClick={() => setModalActive(false)} active={active}>
    <ModalContent onClick={e => e.stopPropagation()}>
      <PictureWrap type={detailPokemon?.types[0]}>
        <img src={detailPokemon?.img} alt={detailPokemon?.name_clean} />
      </PictureWrap>
      <ModalInfo type={detailPokemon?.types[0]}>
        {detailPokemon?.name_clean}
      </ModalInfo>
    </ModalContent>
  </ModalBox>
);

export default Modal;
