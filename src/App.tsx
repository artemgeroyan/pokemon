import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from './components/Header';
import Loading from './components/Loading';

const HomePage = React.lazy(() => import('./pages/HomePage'));
const PokedexPage = React.lazy(() => import('./pages/PokedexPage'));

const App = () => (
  <Suspense fallback={<Loading />}>
    <Header />
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>

      <Route path="/pokedex">
        <PokedexPage />
      </Route>
    </Switch>
  </Suspense>
);

export default App;
