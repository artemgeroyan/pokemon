const baseURL = 'https://zar.hosthot.ru/api/';

export const getAllPokemonsURL = (count: string | number) => `${baseURL}v1/pokemons?limit=12&offset=${count}`;
export const getPokemonByNameURL = (name: string | number) => `${baseURL}v1/pokemons?name=${name}`;
