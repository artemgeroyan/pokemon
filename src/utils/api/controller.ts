import { AxiosRequestConfig } from 'axios';
import { getAllPokemonsURL, getPokemonByNameURL } from './constants';

export const getAllPokemons = (count: string | number): AxiosRequestConfig => ({
  url: getAllPokemonsURL(count),
  method: 'get',
});

export const getPokemonByName = (name): AxiosRequestConfig => ({
  url: getPokemonByNameURL(name),
  method: 'get',
});
