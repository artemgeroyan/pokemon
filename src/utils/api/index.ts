import * as pokemons from './controller';
import { axiosConfig } from './utils';

const apis = {
  axiosWithTokens: {},
  axiosNoTokens: {
    ...pokemons,
  },
};

function axios() {
  return axiosConfig;
}

function createRequest(request, configType) {
  return params => {
    const config = request(params);
    const instanceAxios = axios();
    return instanceAxios[configType](config);
  };
}

function createMethods(apiObj) {
  const methods: any = {};
  Object.keys(apiObj).forEach(apiSection => {
    Object.keys(apiObj[apiSection]).forEach(methodName => {
      methods[methodName] = createRequest(apiObj[apiSection][methodName], apiSection);
    });
  });
  return methods;
}

export default createMethods(apis);
