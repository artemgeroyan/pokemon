import axios from 'axios';

const JSON_CONTENT_TYPE = 'application/json';

const axiosWithTokens = axios.create({
  validateStatus: status => status >= 200 && status < 400,
  headers: {
    accept: JSON_CONTENT_TYPE,
    'Content-Type': JSON_CONTENT_TYPE,
  },
});

const axiosNoTokens = axios.create({
  validateStatus: status => status >= 200 && status < 400,
  headers: {
    accept: JSON_CONTENT_TYPE,
    'Content-Type': JSON_CONTENT_TYPE,
  },
});

export const axiosConfig = {
  axiosWithTokens,
  axiosNoTokens,
};
