import React, { useState, useEffect, useCallback } from 'react';
import {
  Root,
  CardWrapper,
  Title,
  Bold,
  Input,
  InputBlock,
} from './PokedexPage.style';
import useDebounce from '../../hooks/useDebounce';
import PokemonCard from './PokemonCard';
import api from '../../utils/api';
import Modal from '../../components/Modal';

/**
 * Компонент страницы Pokedex. Отображает список покемонов.
 */
const PokedexPage: React.FC = () => {
  const [listOfPokemons, setListOfPokemons] = useState<any>([]);
  const [inputSearchValue, setInputSearchValue] = useState<any>('');
  // eslint-disable-next-line no-unused-vars
  const [isSearching, setIsSearcing] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState<any>(0);
  const [fetching, setFetching] = useState<any>(true);
  const [totalCount, setTotalCount] = useState<any>(1050);
  const [modalActive, setModalActive] = useState<any>(false);
  const [detailPokemon, setDetailPokemon] = useState<any>(null);

  const debouncedValue = useDebounce(inputSearchValue, 500);

  const scrollHandler = useCallback((e: any) => {
    if (e.target.documentElement.scrollHeight
      - (e.target.documentElement.scrollTop + window.innerHeight) < 100
      && listOfPokemons.length < totalCount) {
      setFetching(true);
    }
  }, []);

  const getInitialPokemons = async () => {
    const { data: { pokemons } } = await api.getAllPokemons(0);

    setListOfPokemons(pokemons);
    setCurrentPage((prevState: any) => prevState + 12);
  };

  const getPokemons = async () => {
    const { data: { pokemons, total } } = await api.getAllPokemons(currentPage);

    setListOfPokemons([...listOfPokemons, ...pokemons]);
    setCurrentPage((prevState: any) => prevState + 12);
    setTotalCount(total);
  };

  useEffect(() => {
    if (fetching && !inputSearchValue) {
      try {
        getPokemons();
      } finally {
        setFetching(false);
      }
    }
  }, [fetching, inputSearchValue]);

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler);

    return () => document.removeEventListener('scroll', scrollHandler);
  }, []);

  const handleSearchChange = async (name: string) => {
    try {
      setIsSearcing(true);
      const { data: { pokemons } } = await api.getPokemonByName(name);
      setListOfPokemons(pokemons);
      setIsSearcing(false);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    if (debouncedValue) {
      handleSearchChange(debouncedValue);
    } else {
      getInitialPokemons();
    }
  }, [debouncedValue]);

  const getDetailPokemonHandler = async name => {
    if (name) {
      try {
        // setIsSearcing(true);
        const { data: { pokemons } } = await api.getPokemonByName(name);
        setDetailPokemon(pokemons[0]);
        setModalActive(true);
        // setListOfPokemons(pokemons);
        // setIsSearcing(false);
      } catch (e) {
        console.error(e);
      }
    } else {
      setModalActive(null);
    }
  };

  return (
    <Root>
      <Title>
        <div>
          <Bold>Pokemons</Bold>
          {' '}
          for you to choose our favourite
          {' '}
        </div>
      </Title>
      <InputBlock>
        <Input type="text" onChange={e => setInputSearchValue(e.target.value)} />
      </InputBlock>
      <CardWrapper>
        {listOfPokemons?.map((pokemon: any) => (
          <PokemonCard key={`${pokemon.name_clean}`} pokemon={pokemon} getDetailPokemonHandler={getDetailPokemonHandler} />
        ))}
      </CardWrapper>
      <Modal active={modalActive} setModalActive={setModalActive} detailPokemon={detailPokemon} />
    </Root>
  );
};

export default PokedexPage;
