import styled from 'styled-components';

const BACKGROUND_COLOR: any = {
  water: 'linear-gradient(270deg, #5BC7FA 0.15%, #35BAFF 100%)',
  fire: 'linear-gradient(270deg, #b33327 0.15%, #d93e30 100%)',
  grass: 'linear-gradient(270deg, #64D368 0.15%, #64D368 70.88%)',
};

export const Root = styled.div`
  width: 351px;
  height: 146px;
  box-shadow: 4px 4px 4px rgba(33, 33, 33, 0.1);
  background: #f6f7f9;
  border-radius: 8px;
  overflow: hidden;
  position: relative;
  margin-bottom: 45px;
  cursor: pointer;
  transition: all ease-in-out 0.15s;

  &:hover {
    transform: scale(1.03);
    transition: all ease-in-out 0s;
  }
`;

export const InfoWrap = styled.div`
  position: absolute;
  z-index: 1;
  width: 150px;
  top: -5px;
  left: 25px;
`;

export const TitleName = styled.h1`
  font-size: 24px;
  margin-bottom: 12px;
  text-shadow: 4px 4px 4px rgba(33, 33, 33, 0.1);
`;

export const StatWrap = styled.div`
  display: flex;
  margin-bottom: 12px;
`;

export const StatItem = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: #4b4b4b;
  margin-right: 12px;

  &:last-child {
    margin-right: 0;
  }
`;

export const StatValue = styled.div`
  width: 36px;
  height: 36px;
  border: 3px solid #212121;
  border-radius: 50%;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 18px;
  color: #212121;
  margin: 0 auto 8px;
`;

export const PictureWrap: any = styled.div`
  width: 232px;
  height: 100%;
  background: ${(props: any) => BACKGROUND_COLOR[props.type]};
  position: absolute;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    width: 80%;
  }
`;

export const Label = styled.span`
  background: #f28f16;
  box-shadow: inset 0px -2px 0px rgba(0, 0, 0, 0.18);
  border-radius: 11px;
  height: 15px;
  min-width: 60px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: #212121;
  padding: 0 12px 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 12px;

  &:last-child {
    margin-right: 0;
  }
`;

export const LabelWrap = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

export const SubValue = styled.span`
  color: #4B4B4B;
  font-size: 12px;
`;
