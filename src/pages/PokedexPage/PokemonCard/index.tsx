import React from 'react';
import {
  Root,
  InfoWrap,
  TitleName,
  StatWrap,
  StatItem,
  StatValue,
  PictureWrap,
  Label,
  LabelWrap,
  SubValue,
} from './PokemonCard.style';

export interface IPokemon {
  // eslint-disable-next-line no-unused-vars
  getDetailPokemonHandler: (name) => void;
  pokemon: {
    // eslint-disable-next-line camelcase
    name_clean: string;
    abilities: string[];
    stats: {
      hp: number;
      attack: number;
      defense: number;
      'special-attack': number;
      'special-defense': number;
      speed: number;
    };
    types: string[];
    img: string;
    name: string;
    // eslint-disable-next-line camelcase
    base_experience: number;
    height: number;
    id: number;
    // eslint-disable-next-line camelcase
    is_default: boolean;
    order: number;
    weight: number;
  };
}

const PokemonCard: React.FC<IPokemon> = ({ pokemon, getDetailPokemonHandler }) => (
  <Root onClick={() => getDetailPokemonHandler(pokemon.name_clean)}>
    <InfoWrap>
      <TitleName>{pokemon.name_clean}</TitleName>
      <StatWrap>
        <StatItem>
          <StatValue>{pokemon.stats.attack}</StatValue>
          <SubValue>Attack</SubValue>
        </StatItem>
        <StatItem>
          <StatValue>{pokemon.stats.defense}</StatValue>
          <SubValue>Defense</SubValue>
        </StatItem>
      </StatWrap>
      <LabelWrap>
        {pokemon.types.map(el => (
          <Label key={el}>{el}</Label>
        ))}
      </LabelWrap>
    </InfoWrap>
    <PictureWrap type={pokemon.types[0]}>
      <img src={pokemon.img} alt={pokemon.name_clean} />
    </PictureWrap>
  </Root>
);

export default PokemonCard;
