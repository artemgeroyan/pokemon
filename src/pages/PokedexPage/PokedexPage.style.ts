import styled from 'styled-components';

export const Root = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const CardWrapper = styled.div`
  display: grid;
  margin: 0 auto;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 2vw;
  max-width: 1125px;
`;

export const Title = styled.h1`
  margin-top: 73px;
  font-weight: normal;
  margin-bottom: 30px;
`;

export const Bold = styled.span`
  font-weight: bold;
`;

export const InputBlock = styled.div`
  text-align: center;
  width: 100%;
  margin-bottom: 100px;
`;

export const Input = styled.input`
  width: 70%;
  background: linear-gradient(180deg, #ffffff 30.32%, #f5f5f5 100%);
  border: none;
  outline: none;
  border-radius: 10px;
  height: 48px;
  box-shadow: 6px 11px 27px -8px rgba(0, 0, 0, 0.35);
`;
