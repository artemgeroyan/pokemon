import styled from 'styled-components';

export const Page = styled.div`
  height: 100vh;
  background: linear-gradient(180deg, #f5db13 0%, #f2b807 100%);
  overflow-y: hidden;
`;

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  max-width: 1280px;
  padding: 0 75px;
  margin: 0 auto;
`;

export const TextBlock = styled.div`
  max-width: 520px;
`;

export const TitleSpan = styled.span`
  font-weight: normal;
`;

export const Subtitle = styled.p`
  font-size: 32px;
  line-height: 37px;
`;

export const HeadingBlock = styled.h1`
  font-size: 72px;
`;
