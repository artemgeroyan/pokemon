import React, { useState, useEffect } from 'react';
import {
  Root,
  SmallPokeBall,
  Cloud,
  CloudBig,
  PokeBall,
  Pikachu,
} from './Parallax.style';
import SmallPokeBallPng from './assets/PokeBall1.png';
import CloudPng from './assets/Cloud1.png';
import PokeBallPng from './assets/PokeBall2.png';
import CloudBigPng from './assets/Cloud2.png';
import PikachuPng from './assets/Pikachu.png';

const Parallax = () => {
  const [screenX, setScreenX] = useState(0);
  const [screenY, setScreenY] = useState(0);

  useEffect(() => {
    const handleMouseMove = (event: MouseEvent) => {
      setScreenX(event.screenX);
      setScreenY(event.screenY);
    };
    window.addEventListener('mousemove', handleMouseMove);

    return () => removeEventListener('mousemove', handleMouseMove);
  }, [screenX, screenY]);
  return (
    <Root>
      <SmallPokeBall>
        <img
          src={SmallPokeBallPng}
          alt="Small PokeBall"
          style={{
            transform: `translate(${screenY * 0.03}px,${screenX * 0.06}px )`,
          }}
        />
      </SmallPokeBall>
      <Cloud>
        <img
          src={CloudPng}
          alt="Cloud PokeBall"
          style={{
            transform: `translate(${screenY * 0.03}px,${screenX * 0.09}px )`,
          }}
        />
      </Cloud>
      <CloudBig>
        <img
          src={CloudBigPng}
          alt="Cloud Big PokeBall"
          style={{
            transform: `translate(${screenY * 0.03}px,${screenX * 0.03}px )`,
          }}
        />
      </CloudBig>

      <PokeBall>
        <img
          src={PokeBallPng}
          alt="Big PokeBall"
          style={{
            transform: `translate(${screenY * 0.03}px,${screenX * 0.01}px )`,
          }}
        />
      </PokeBall>
      <Pikachu>
        <img
          src={PikachuPng}
          alt="Cloud PokeBall"
          style={{
            transform: `translate(${screenY * 0.03}px,${screenX * 0.13}px )`,
          }}
        />
      </Pikachu>
    </Root>
  );
};

export default Parallax;
