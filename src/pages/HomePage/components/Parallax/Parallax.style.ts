import styled from 'styled-components';

export const Root = styled.div`
  width: 793px;
  height: 580px;
  position: relative;
  margin-right: -119px;
`;

export const SmallPokeBall = styled.div`
  position: absolute;
  top: 1%;
  left: 1%;
`;

export const Cloud = styled.div`
  position: absolute;
  left: 12.31%;
  bottom: 39.22%;
`;

export const CloudBig = styled.div`
  position: absolute;
  left: 49.86%;
  top: -3%;
`;

export const PokeBall = styled.div`
  position: absolute;
  top: 24.38%;
  right: -12%;
`;

export const Pikachu = styled.div`
  position: absolute;
  top: 0%;
  left: 4.11%;
`;
