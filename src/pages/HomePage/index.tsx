import React from 'react';
import {
  Page,
  Wrapper,
  TextBlock,
  HeadingBlock,
  Subtitle,
  TitleSpan,
} from './Home.style';
import Button from '../../components/Button';
import Parallax from './components/Parallax';

const HomePage = () => (
  <Page>
    <Wrapper>
      <TextBlock>
        <HeadingBlock>
          Find
          <TitleSpan> all your favorite</TitleSpan>
          {' '}
          Pokemon
        </HeadingBlock>
        <Subtitle>
          You can know the type of Pokemon, its strengths, disadvantages and
          abilities
        </Subtitle>
        <Button isGreen onClick={() => console.error('eee')}>
          See pokemons
        </Button>
      </TextBlock>
      <Parallax />
    </Wrapper>
  </Page>
);

export default HomePage;
