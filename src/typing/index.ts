// eslint-disable-next-line no-shadow
export enum LinkEnum {
  // eslint-disable-next-line no-unused-vars
  HOME = '/',
  // eslint-disable-next-line no-unused-vars
  POKEDEX = '/pokedex',
  // eslint-disable-next-line no-unused-vars
  LEGENDARIES = '/legendaries',
  // eslint-disable-next-line no-unused-vars
  DOCUMENTATION = '/documentation',
  // eslint-disable-next-line no-unused-vars
  POKEMON = '/pokedex/:id',
}

interface IGeneralMenu {
  title: string;
  link: LinkEnum;
  // component: (props: PropsWithChildren<any>) => JSX.Element;
}

export const GENERAL_MENU: IGeneralMenu[] = [
  {
    title: 'Home',
    link: LinkEnum.HOME,
    // component: () => <HomePage />,
  },
  {
    title: 'Pokedex',
    link: LinkEnum.POKEDEX,
    // component: () => <PokedexPage />,
  },
  {
    title: 'Legendaries',
    link: LinkEnum.LEGENDARIES,
    // component: () => <HomePage />,
  },
  {
    title: 'Documentation',
    link: LinkEnum.DOCUMENTATION,
    // component: () => <HomePage />,
  },
];
